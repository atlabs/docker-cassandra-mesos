#!/bin/bash
set -e

# In case first arg is not cassandra but instead is a switch like `-f` or `--some-option`
if [ "${1:0:1}" = '-' ]; then
	set -- cassandra -f "$@"
	echo "Parameters are now $@..."
fi

# allow the container to be started with `--user`
if [ "$1" = 'cassandra' -a "$(id -u)" = '0' ]; then
	echo "Setting cassandra-env.sh..."
	: ${CASSANDRA_JMX="true"}
	if [ "${CASSANDRA_JMX}" == "true" ]; then
		export LOCAL_JMX="false"
		: ${CASSANDRA_JMX_USER="cassandra"}
		: ${CASSANDRA_JMX_PASSWORD="cassandra"}
		CASSANDRA_PW_FILE="/etc/cassandra/jmxremote.password"
		echo "Enabling remote JMX with ${CASSANDRA_JMX_USER}/${CASSANDRA_JMX_PASSWORD}"
		echo "${CASSANDRA_JMX_USER} ${CASSANDRA_JMX_PASSWORD}" > ${CASSANDRA_PW_FILE}
        chown cassandra:cassandra ${CASSANDRA_PW_FILE}
        chmod 400 ${CASSANDRA_PW_FILE}
		: ${JAVA_HOME=`type -p java|xargs readlink -f|xargs dirname|xargs dirname`}
		JMX_PERMISSION_FILE="${JAVA_HOME}/lib/management/jmxremote.access"
		echo "${CASSANDRA_JMX_USER} readwrite" >> ${JMX_PERMISSION_FILE}
	fi
	echo "Updating permissions on /var/lib/cassandra /var/log/cassandra ${CASSANDRA_CONFIG}..."
	chown -R cassandra /var/lib/cassandra /var/log/cassandra "${CASSANDRA_CONFIG}"
	echo "Executing '$BASH_SOURCE $@' as user cassandra..."
	exec gosu cassandra "$BASH_SOURCE" "$@"
fi

if [ "$1" = 'cassandra' ]; then

	echo "Processing cassandra configuration..."

	: ${CASSANDRA_RPC_ADDRESS='0.0.0.0'}

	: ${CASSANDRA_LISTEN_ADDRESS='auto'}
	if [ "${CASSANDRA_LISTEN_ADDRESS}" = 'auto' ]; then
		CASSANDRA_LISTEN_ADDRESS="$(hostname --ip-address)"
	fi

	: ${CASSANDRA_BROADCAST_ADDRESS="$CASSANDRA_LISTEN_ADDRESS"}

	if [ "${CASSANDRA_BROADCAST_ADDRESS}" = 'auto' ]; then
		CASSANDRA_BROADCAST_ADDRESS="$(hostname --ip-address)"
	fi
	: ${CASSANDRA_BROADCAST_RPC_ADDRESS:=${CASSANDRA_BROADCAST_ADDRESS}}

	if [ -n "${CASSANDRA_NAME:+1}" ]; then
		: ${CASSANDRA_SEEDS:="cassandra"}
	fi


    if [ -z "${CASSANDRA_SEEDS}" ]; then
		echo "Setting seeds to default ${CASSANDRA_BROADCAST_ADDRESS}"
		CASSANDRA_SEEDS="${CASSANDRA_BROADCAST_ADDRESS}"
    else
		if [[ ${CASSANDRA_SEEDS} =~ "\b(?:\d{1,3}\.){3}\d{1,3}\b" ]]; then
        	echo "Setting seeds to default ${CASSANDRA_SEEDS}"
        else
        	echo "Resolving ${CASSANDRA_SEEDS}..."
        	SEED_LIST=`echo $(dig +short ${CASSANDRA_SEEDS}) | tr ' ' ','`
		    if [ -z "${SEED_LIST}" ]; then
				echo "Could not resolve ${CASSANDRA_SEEDS}, setting seeds to ${CASSANDRA_BROADCAST_ADDRESS}"
				CASSANDRA_SEEDS="${CASSANDRA_BROADCAST_ADDRESS}"
			else
				echo "Seeds resolved to ${SEED_LIST}."
				CASSANDRA_SEEDS=${SEED_LIST}
			fi
		fi
	fi

	sed -ri 's/(- seeds:).*/\1 "'"$CASSANDRA_SEEDS"'"/' "${CASSANDRA_CONFIG}/cassandra.yaml"

	for yaml in \
		broadcast_address \
		broadcast_rpc_address \
		cluster_name \
		endpoint_snitch \
		listen_address \
		num_tokens \
		rpc_address \
		start_rpc \
	; do
		var="CASSANDRA_${yaml^^}"
		val="${!var}"
		if [ "${val}" ]; then
			sed -ri 's/^(# )?('"${yaml}"':).*/\2 '"${val}"'/' "${CASSANDRA_CONFIG}/cassandra.yaml"
		fi
	done

	for rackdc in dc rack; do
		var="CASSANDRA_${rackdc^^}"
		val="${!var}"
		if [ "${val}" ]; then
			sed -ri 's/^('"${rackdc}"'=).*/\1 '"${val}"'/' "${CASSANDRA_CONFIG}/cassandra-rackdc.properties"
		fi
	done

	echo "Setting jvm.options..."

    : ${CASSANDRA_HEAP="none"}
	if [ "${CASSANDRA_HEAP}" = "none" ]; then
        CASSANDRA_HEAP=`/calculate-heap-limit.sh`
	fi

	if [ "${CASSANDRA_HEAP}" != "default" ]; then
        echo "Cassandra heap size set to ${CASSANDRA_HEAP}..."
		echo "-Xms${CASSANDRA_HEAP}" >> ${CASSANDRA_CONFIG}/jvm.options
		echo "-Xmx${CASSANDRA_HEAP}" >> ${CASSANDRA_CONFIG}/jvm.options
    fi

	echo "Configuration complete."

fi

echo "Executing '$@'..."
exec "$@"
