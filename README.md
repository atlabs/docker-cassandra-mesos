# docker-cassandra-mesos
Cassandra with Oracle JDK, G1 enabled, and dynamic setting of heap memory based on 0.75 available from cgroups. 

To use this image with Mesos, you will need:

 - A running Mesos cluster
 - Mesos DNS installed
 - A Marathon installation
 - Mesos slave configured with Docker container

Build this image, and push it to your registry. Then configure your Cassandra Marathon tasks to launch Cassandra seed nodes
and regular nodes.


# Building and Pushing

Assuming your private registry is called `my-registry:5010`, create this image as follows:

	sudo docker build -t atlp/cassandra-mesos:3.0 3.0/.

Now that our image is built, push it to your registry:

	sudo docker push atlp/cassandra-mesos:3.0


# Environment Variables

The generated docker image supports all the same parameters as the [official version](https://hub.docker.com/r/_/cassandra/).
The main difference is that, when using something other than an IP number for `CASSANDRA_SEEDS`, the image will first
try to resolve the DNS name to a list of IP numbers. This works well when using Mesos DNS. If the DNS name
cannot be resolved, then the value in `CASSANDRA_BROADCAST_ADDRESS` will be used instead.

In addition, remote JMX can be enabled by setting `CASSANDRA_JMX` to `true`. When setting this variable to true, you
may also want to set `CASSANDRA_JXM_USER` and `CASSANDRA_JMX_PASSWORD`. If you do not set these two, the default value 
for both will be `cassandra`. 

	$ sudo docker run --net host -e CASSANDRA_BROADCAST_ADDRESS=auto -e CASSANDRA_JMX=true atlp/cassandra-mesos:3.0

Metrics are by default accessible at url http://<hostname>:7070/metrics. The port used can be changed by setting
environment variable `METRICS_PORT`:

	$ docker run -p 9042:9042 -p 7199:7199 -p 7070:7070 -e METRICS_PORT=7070 atlp/cassandra-mesos:3.0

# Marathon Task

If you have a small cluster of say 3 cassandra instances, you can use the included `cassandra-marathon.json`
configuration as is. However, if you have more than 3 instances, it is recommended to keep the seeds to
a small number (e.g. 3). In those cases, create an application like `cassandra/seed` and an application
`cassandra/node` to scale all the nodes.


